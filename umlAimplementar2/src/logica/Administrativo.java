package logica;

import java.util.Date;

public class Administrativo extends Usuario {
	private String areadTrabaj;
	private int sueldo;
	
	public Administrativo(int id, String nombre, Date fechaNac, String areadTrabaj, int sueldo, Hospital hosp) {
		super(id, nombre, fechaNac, hosp);
		this.areadTrabaj = areadTrabaj;
		this.sueldo = sueldo;
	}

	public String getAreadTrabaj() {
		return areadTrabaj;
	}

	public void setAreadTrabaj(String areadTrabaj) {
		this.areadTrabaj = areadTrabaj;
	}

	public int getSueldo() {
		return sueldo;
	}

	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}
	
	public int calcularSueldo() {
		return sueldo;
	}
	
	
	
}
