package logica;

import java.util.ArrayList;
import java.util.Date;

public class Manejador {
	private ArrayList<Usuario> users;
	private ArrayList<Hospital> hosps;
	int iU;
	int iH;
	
	public Manejador() {
		super();
		this.users = new ArrayList<>();
		this.hosps = new ArrayList<>();
		iU = 0;
		iH = 0;
	}
	
	public Hospital getHosp(int id) {
		int i = 0;
		while ((i < this.hosps.size()) && (id != this.hosps.get(i).getId())) {
			i++;
		}
		
		if (i < this.hosps.size()) {
			return this.hosps.get(i);
		} else {
			return null;
		}
	}

	public void addHosp(Hospital hosp) {
		this.hosps.add(hosp);
	}
	
	public Usuario getUser(int id) {
		int i = 0;
		while ((i < this.users.size()) && (id != this.users.get(i).getId())) {
			i++;
		}
		
		if (i < this.users.size()) {
			return this.users.get(i);
		} else {
			return null;
		}
	}

	public void addUsuario(Usuario user) {
		this.users.add(user);
	}
	
	public void altaMedico(String nombre, Date fechaNac, String especialidad, int sueldo, int idHosp) {
		Usuario user = new Medico(iU, nombre, fechaNac, especialidad, sueldo, getHosp(idHosp));
		getHosp(idHosp).addUsuario(user);
		this.users.add(user);
		iU++;
	}
	
	public void altaAdministrativo(String nombre, Date fechaNac, String areadTrabaj, int sueldo, int idHosp) {
		Usuario user = new Administrativo(iU, nombre, fechaNac, areadTrabaj, sueldo, getHosp(idHosp));
		getHosp(idHosp).addUsuario(user);
		this.users.add(user);
		iU++;
	}
	
	public void altaCliente(String nombre, Date fechaNac, int cuota, int idHosp) {
		Usuario user = new Cliente(iU, nombre, fechaNac, cuota, getHosp(idHosp));
		getHosp(idHosp).addUsuario(user);
		this.users.add(user);
		iU++;
	}
	
	public void altaHospital(String direccion, boolean servicioEme, Date fechaInau) {
		Hospital hosp = new Hospital(iH, direccion, servicioEme, fechaInau);
		this.hosps.add(hosp);
		iH++;
	}
	
	public void bajaMedico(int idMed) {
		ArrayList<Hospital> hospMed = ((Medico) getUser(idMed)).getHosps();
		
		// Mato las relaciones Hosp -> Med
		int i = 0;
		while (i < hospMed.size()) {
			hospMed.get(i).bajaUsuario(idMed);
			i++;
		}
		
		// Saco al Med del AL users
		i = 0;
		while ((i < users.size()) && (idMed != users.get(i).getId())) {
			i++;
		}
		
		if (i < users.size()) {
			users.remove(i);
		}

		
	}
	
	public void bajaAdministrativo(int idAdm) {
		ArrayList<Hospital> hospAdm = ((Administrativo) getUser(idAdm)).getHosps();
		
		// Mato las relaciones Hosp -> Adm
		int i = 0;
		while (i < hospAdm.size()) {
			hospAdm.get(i).bajaUsuario(idAdm);
			i++;
		}
		
		// Saco al Adm del AL users
		i = 0;
		while ((i < users.size()) && (idAdm != users.get(i).getId())) {
			i++;
		}
		
		if (i < users.size()) {
			users.remove(i);
		}
	}
	
	public void bajaCliente(int idCli) {
		ArrayList<Hospital> hospCli = ((Cliente) getUser(idCli)).getHosps();
		
		// Mato las relaciones Hosp -> Cli
		int i = 0;
		while (i < hospCli.size()) {
			hospCli.get(i).bajaUsuario(idCli);
			i++;
		}
		
		// Saco al Cli del AL users
		i = 0;
		while ((i < users.size()) && (idCli != users.get(i).getId())) {
			i++;
		}
		
		if (i < users.size()) {
			users.remove(i);
		}
	}
	
	public int hospitalMasRentable() {
		int i = 0;
		int idHosp = 0;
		int acumulador = 0;
		while (i < this.hosps.size()) {
			if (acumulador < this.hosps.get(i).calcularLiquidacion()) {
				acumulador = this.hosps.get(i).calcularLiquidacion();
				idHosp = this.hosps.get(i).getId();
			}
			i++;
		}
		
		return idHosp;
	}
	
	public int hospitalMenosRentable() {
		int i = 0;
		int idHosp = 0;
		int acumulador = 0;
		while (i < this.hosps.size()) {
			if (acumulador >= this.hosps.get(i).calcularLiquidacion()) {
				acumulador = this.hosps.get(i).calcularLiquidacion();
				idHosp = this.hosps.get(i).getId();
			}
			i++;
		}
		
		return idHosp;
	}

	public void imprimirEstatusHospitales() {
		int i = 0;
		while (i < this.hosps.size()) {
			System.out.println("H" + this.hosps.get(i).getId() + ": "
					+ this.hosps.get(i).getDireccion() + " #Meds: "
					+ this.hosps.get(i).cantMedicos() + " #Cli: "
					+ this.hosps.get(i).cantCliente() + " #Adms: "
					+ this.hosps.get(i).cantAdministr() + " Liq: "
					+ this.hosps.get(i).calcularLiquidacion() + " #Esp: "
					+ this.hosps.get(i).cantEspecialidades());
			
			i++;
		}
		System.out.println("\n");
	}

	public void imprimirEstatusUsuarios() {
		int i = 0;
		while (i < this.users.size()) {
			System.out.println("U" + this.users.get(i).getId() + ": "
					+ this.users.get(i).getNombre() + " #Hosps: "
					+ this.users.get(i).cantHosps() + " Sueld: "
					+ this.users.get(i).calcularSueldo());
			
			i++;
		}
		System.out.println("\n");
	}
	
	public void imprimirEstatusGens() {
		System.out.println("Hosp + Rent: " + hospitalMasRentable());
		System.out.println("Hosp - Rent: " + hospitalMenosRentable());

		System.out.println("\n");
	}

}















