package logica;

import java.util.Date;

public class Cliente extends Usuario {
	private int cuota;

	public Cliente(int id, String nombre, Date fechaNac, int cuota, Hospital hosp) {
		super(id, nombre, fechaNac, hosp);
		this.cuota = cuota;
	}

	public int getCuota() {
		return cuota;
	}

	public void setCuota(int cuota) {
		this.cuota = cuota;
	}
	
	public int calcularSueldo() {
		return -1*cuota;
	}
	
	
}
